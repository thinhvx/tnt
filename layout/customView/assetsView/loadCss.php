<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

<link href="css/style.css" rel="stylesheet" />
<link href="css/responsive.css" rel="stylesheet" />
<link href="css/tooltip.css" rel="stylesheet" />
<link rel="stylesheet" id="fontawesome-css" href="css/storyline/font-awesome/css/font-awesome.css" type="text/css" media="all" />
<link rel="stylesheet" id="prettyPhoto-css" href="css/storyline/prettyPhoto.css" type="text/css" media="all" />
<link rel="stylesheet" id="flexslider-css" href="css/storyline/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" id="mainstyle-css" href="css/storyline/style.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="js/storyline/audioplayer/360player.css" />
<link rel="stylesheet" type="text/css" href="js/storyline/audioplayer/360player-visualization.css" />

<link rel="stylesheet" type="text/css" href="css/camera.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/jcarousel.responsive.css">
<link rel="stylesheet" type="text/css" href="css/fdw.css">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
