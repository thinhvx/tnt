<div style="display:none">
    <script src="js/jquery-1.8.2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/camera.min.js"></script>
    <script src="js/popup.js"></script>

    <script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="js/jcarousel.responsive.js"></script>

    <script type="text/javascript" src="js/jquery-hover-effect.js"></script>

    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script src="js/init.js"></script>

    <!---- story line plugin -->
    <script type="text/javascript" src="js/storyline/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="js/storyline/modernizr.custom.79639.js"></script>
    <script type="text/javascript" src="js/storyline/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/storyline/all-functions.js"></script>
    <script type="text/javascript" src="js/storyline/classList.js"></script>
    <script type="text/javascript" src="js/storyline/bespoke.js"></script>
    <script type="text/javascript" src="js/storyline/jquery.flexslider.js"></script>
    <script>
        jQuery(document).ready(function ($){scrollinit("carousel", 3, 1, true, true, true, true, false);});
    </script>
</div>