<div id="event">
    <div class="container">
        <h1>Nổi Bật</h1>

        <div id="ss-holder" class="ss-holder" style="height:500px;position:relative">
            <div id="effects">
                <article id="articlehold">
                    <section><div class="ss-row turquoise go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event1.png" rel="prettyPhotoImages[1]"><img src="images/event1.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Turquoise color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt <a href="#"><strong>Read more</strong></div></div></section><section><div class="ss-row greensea go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event2.png" rel="prettyPhotoImages[1]"><img src="images/event2.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Green Sea color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <a href="#"><strong>Read more</strong></div></section><section><div class="ss-row emerald go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event3.png" rel="prettyPhotoImages[1]"><img src="images/event3.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Emerald color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad  <a href="#"><strong>Read more</strong></div></section><section><div class="ss-row nephritis go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event1.png" rel="prettyPhotoImages[1]"><img src="images/event1.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Nephritis color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  <a href="#"><strong>Read more</strong></div></section><section><div class="ss-row peterriver go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event2.png" rel="prettyPhotoImages[1]"><img src="images/event2.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Peter River color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  <a href="#"><strong>Read more</strong></div></section><section><div class="ss-row belizehole go-anim"><div class="ribbon"><i class="icon-time icon-large"></i> 29,July <div class="seclevelribbon"><div class="thirdlevelribbon"><div class="ribbon-sec">2013</div></div></div></div><div class="hover-effect h-style"><a href="images/event3.png" rel="prettyPhotoImages[1]"><img src="images/event3.png" class="clean-img" /><div class="mask"><i class="icon-search"></i><span class="img-rollover"></span></div></a></div><div class="ss-container"><h3 class="content-title"><a href="#">Belize Hole color scheme</a></h3><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo  ... <a href="#"><strong>Read more</strong></div></section>

                </article>
                <div class="ss-nav-arrows-next"><i id="next-arrow" style="opacity:0" class="icon-angle-right "></i></div><div class="ss-nav-arrows-prev" style=""><i id="prev-arrow" style="opacity:0" class="icon-angle-left"></i></div></div></div><div class="ss-holder"><div class="inifiniteLoader animated fadeOutDown"><div class="bar"><span></span></div></div></div>
        <!-- OLD "NOI BAT"

<div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
        <li class="event_item">
            <a href="#" title="">
                <img src="images/event1.png" alt="" />
             </a>
             <a class="title_event" href="#" title="">VIOS 2014</a>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing,
vivamus congue nulla leo, quis imperdiet magna.</p>
        </li>
        <li class="event_item">
            <a href="#" title="">
                <img src="images/event2.png" alt="" />
             </a>
             <a class="title_event" href="#" title="">FORTUNER TRD 2014</a>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing,
vivamus congue nulla leo, quis imperdiet magna.</p>
        </li>
        <li class="event_item">
            <a href="#" title="">
                <img src="images/event3.png" alt="" />
             </a>
             <a class="title_event" href="#" title="">YARIS 2014</a>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing,
vivamus congue nulla leo, quis imperdiet magna.</p>
        </li>
        <li class="event_item">
            <a href="#" title="">
                <img src="images/event1.png" alt="" />
             </a>
             <a class="title_event" href="#" title="">VIOS 2014</a>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing,
vivamus congue nulla leo, quis imperdiet magna.</p>
        </li>
        <li class="event_item">
            <a href="#" title="">
                <img src="images/event2.png" alt="" />
             </a>
             <a class="title_event" href="#" title="">FORTUNER TRD 2014</a>
             <p>Lorem ipsum dolor sit amet, consectetur adipiscing,
vivamus congue nulla leo, quis imperdiet magna.</p>
        </li>
         </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>
            </div>
-->
    </div>
</div>