<div id="news" class="container">
    <h1 class="title_news">Tin Tức</h1>
    <div class="col_news">
        <div class="item_news">
            <a class="click_news" href="#" title="">
                <img src="images/news1.png" alt="" />
                <div class="bg_news"></div>
                <article class="da-animate da-slideFromRight" style="display: block;">
                    <!--<div style="opacity: 0;" class="fdw-background">
                        <p class="fdw-port">
                            <a href="#" title="">Đọc tiếp</a>
                        </p>
                    </div>-->
                </article>
            </a>
            <div class="content_news">
                <h3 class="category">Sự kiện</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">Toyota Nha Trang nhận bằng Đại lý chính
                        thức từ Toyota Motor Việt Nam</a></h4>
                <p>Toyota Nha Trang đã được lãnh đạo của Toyota Motor Việt Nam trao bằng
                    công nhận là đại lý chính thức của Toyota Motor Việt Nam cùng với sự đồng
                    thuận của lãnh đạo từ các đại lý khác có mặt trong buổi họp đại lý ngày 19/08
                    vừa qua tại thành phố biển Đà Nẵng xinh đẹp.</p>
            </div>
        </div>

        <div class="item_news">
            <a class="click_news" href="#" title="">
                <img src="images/news2.png" alt="" />
                <div class="bg_news"></div>
                <article class="da-animate da-slideFromRight" style="display: block;">
                </article>
            </a>
            <div class="content_news">
                <h3 class="category">Khuyến Mãi</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">Ưu đãi cực sốc với chương trình phụ kiện
                        cuối năm</a></h4>
                <p>Khách hàng khi lắp đặt phụ kiện tại Toyota Nha Trang sẽ nhận được quà tặng
                    và voucher có giá trị lên đến 2.000.000 đồng.</p>
            </div>
        </div>

    </div>
    <!-- end col -->
    <div class="col_news">
        <div class="item_news">
            <a class="click_news" href="#" title="">
                <img src="images/news3.png" alt="" />
                <div class="bg_news"></div>
                <article class="da-animate da-slideFromRight" style="display: block;">
                </article>
            </a>
            <div class="content_news">
                <h3 class="category">Mẫu xe lạ</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">Toyota 86 sẽ có phiên bản sedan
                        hoàn toàn mới</a></h4>
                <p>Phiên bản concept của Toyota 86 Sedan sẽ chính thức được trình làng
                    trong triển lãm Dubai 2013 diễn ra vào tháng này
                    Những tin đồn về kế hoạch bổ sung phiên bản sedan cho dòng xe coupe
                    thể thao dẫn động cầu sau 86 của Toyota đã bắt đầu xuất hiện cách đây
                    không lâu. </p>
            </div>
        </div>

        <div class="item_news">
            <a class="click_news" href="#" title="">
                <object width="355" height="200"><param name="movie" value="//www.youtube.com/v/xQ6DeFKpsDs?version=3&amp;hl=vi_VN"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/xQ6DeFKpsDs?version=3&amp;hl=vi_VN" type="application/x-shockwave-flash" width="355" height="200" allowscriptaccess="always" allowfullscreen="true"></embed></object>
                <div class="bg_news"></div>
            </a>
            <div class="content_news">
                <h3 class="category">Hoạt động nội bộ</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">Quốc tế Thiếu nhi - Chia sẻ yêu thương</a></h4>
                <p>Toyota Nha Trang luôn nỗ lực lao động phát triển cùng với tinh thần gắn kết,
                    chia sẻ với cộng đồng, góp phần nhỏ của mình cho cuộc sống tươi đẹp hơn.</p>
            </div>
        </div>

    </div>
    <!-- end col -->
    <div class="col_news">
        <div class="item_news">
            <a class="click_news" href="#" title="">
                <img src="images/news5.png" alt="" />
                <div class="bg_news"></div>
                <article class="da-animate da-slideFromRight" style="display: block;">
                </article>
            </a>
            <div class="content_news">
                <h3 class="category">Tuyển Dụng</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">TOYOTA NHA TRANG TUYỂN DỤNG
                        NHÂN SỰ THÁNG 6/2014</a></h4>
                <p>Toyota Nha Trang là đại lý Ủy quyền chính thức của Công ty sản xuất ôtô
                    hàng đầu tại Việt Nam – Toyota Motor Việt Nam. Công ty chúng tôi Kinh
                    doanh ô tô mới, phụ tùng chính hiệu Toyota và dịch vụ sửa chữa, bảo dưỡng,
                    bảo hành ô tô. Để đáp ứng sự phát triển ngày càng lớn mạnh, Toyota Nha
                    Trang có nhu cầu tuyển dụng nhân sự vào vị trí sau:</p>
            </div>
        </div>

        <div class="item_news">
            <a class="click_news" href="#" title="">
                <img src="images/news6.png" alt="" />
                <div class="bg_news"></div>
                <article class="da-animate da-slideFromRight" style="display: block;">
                </article>
            </a>
            <div class="content_news">
                <h3 class="category">Xe mới</h3>
                <div class="control">
                    <div class="date">27 July 2013</div>
                    <div class="view">14 Lượt Xem</div>
                    <div class="comment">14 Comments</div>
                </div>
                <h4><a href="#" title="">Bạn hiểu gì về những ký hiệu trên
                        xe hơi ?</a></h4>
                <p>Mỗi hãng xe đều có cách đặt kí hiệu để phân biệt phẩm cấp xe cho riêng
                    mình, bài viết dưới đây phần nào giúp bạn đọc hiểu thêm.</p>
            </div>
        </div>

    </div>
    <!-- end col -->
</div>