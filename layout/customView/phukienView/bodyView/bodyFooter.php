<div id="footer">
    <div class="container">
        <div class="footer_colum">
            <h2>VE CONG TY</h2>
            <ul>
                <li><a href="#" title="">TONG QUAN</a></li>
                <li><a href="#" title="">THANH TUU</a></li>
                <li><a href="#" title="">PHONG TRUNG BAY</a></li>
                <li><a href="#" title="">THANH VIEN</a></li>
            </ul>
        </div>
        <div class="footer_colum">
            <h2>HO TRO</h2>
            <ul>
                <li><a href="#" title="">faq</a></li>
                <li><a href="#" title="">CHAT</a></li>
                <li><a href="#" title="">LIEN HE</a></li>
                <li><a href="#" title="">SITE MAP</a></li>
            </ul>
        </div>
        <div class="footer_colum" style="margin:0">
            <h2>CHIA SE</h2>
            <div>
                <a href="#" title="">
                    <img src="images/facebook.png" alt=""/>
                </a>
                <a href="mailto:info@toyotanhatrang.com.vn" title="">
                    <img src="images/email.png" alt=""/>
                </a>
                <a href="#" title="">
                    <img src="images/youtube.png" alt=""/>
                </a>
            </div>
        </div>
        <div class="coppyright">
            © BAN QUYEN THUOC VE TOYOTA NHA TRANG
        </div>
    </div>
</div>