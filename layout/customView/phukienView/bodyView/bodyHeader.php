<!-- Begin Header-->
<div id="header">
    <div class="container">
        <div class="navar">
            <a class="hide_navar" href="javascript:void(0)" title="">
            </a>
        </div>
        <div class="search">
            <form action="#" method="get">
                <input type="text" onfocus="if (this.value == 'Search...') {this.value = '';}"
                       onblur="if (this.value == '') {this.value = 'Search...';}" value="Search..."/>
            </form>
        </div>
        <div class="right_header">
            <div class="logo">
                <a href="#" title="Toyota Nha Trang">
                    <img src="images/logo.png" alt="Toyota Nha Trang"/>
                </a>
            </div>
            <div class="social">
                <a id="facebook" href="#face_box" title="">
                    <img src="images/facebook.png" alt=""/>
                </a>
                <a href="mailto:info@toyotanhatrang.com.vn" title="">
                    <img src="images/email.png" alt=""/>
                </a>
                <a href="#" title="">
                    <img src="images/youtube.png" alt=""/>
                </a>

                <div id="face_box">
                    <iframe
                        src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ftoyotanhatrang&amp;width=500&amp;height=350&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true"
                        scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:350px;"
                        allowTransparency="true"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div id="menu">
        <div class="container">
            <div class="l_menu">
            <div class="row_auto">
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/86.png" alt=""/>
                        </a>
                        <h5>Toyota 86</h5>

                        <div class="price">Giá từ 1,6 tỷ</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/86_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Toyota 86</span>
                                    <span>1,678 tỷ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/vios.png" alt=""/>
                        </a>
                        <h5>Vios</h5>

                        <div class="price">Giá từ 538 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/vios_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Vios G</span>
                                    <span>612 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/vios_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Vios E</span>
                                    <span>561 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/vios_3.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Vios J</span>
                                    <span>538 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/yaris.png" alt=""/>
                        </a>
                        <h5>Yaris</h5>

                        <div class="price">Giá từ 620 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/yaris_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Yaris G</span>
                                    <span>669 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/yaris_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Yaris E</span>
                                    <span>620 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="row_auto">
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/altis.png" alt=""/>
                        </a>
                        <h5>Corolla Altis</h5>

                        <div class="price">Giá từ 746 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/altis_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Corolla Altis 2.0RS</span>
                                    <span>914 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/altis_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Corolla Altis 2.0V (CVT)</span>
                                    <span>869 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/altis_3.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Corolla Altis 1.8G (CVT)</span>
                                    <span>799 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/altis_4.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Corolla Altis 1.8G (MT)</span>
                                    <span>746 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/camry.png" alt=""/>
                        </a>
                        <h5>Camry</h5>

                        <div class="price">Giá từ 999 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/camry_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Camry 2.5Q</span>
                                    <span>1,292 tỷ</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/camry_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Camry 2.5G</span>
                                    <span>1,164 tỷ</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/camry_3.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Camry 2.0E</span>
                                    <span>999 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/innova.png" alt=""/>
                        </a>
                        <h5>Innova</h5>

                        <div class="price">Giá từ 710 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/innova_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Innova V</span>
                                    <span>817 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/innova_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Innova G</span>
                                    <span>751 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/innova_3.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Innova E</span>
                                    <span>710 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="row_auto">
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/fortuner.png" alt=""/>
                        </a>
                        <h5>Fortuner</h5>

                        <div class="price">Giá từ 892 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/fortuner_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Fortuner 2.7V (4x4)</span>
                                    <span>1,056 tỷ</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/fortuner_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Fortuner 2.7V (4x2)</span>
                                    <span>950 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/fortuner_3.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Fortuner 2.5G (4x2)</span>
                                    <span>892 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/trd.png" alt=""/>
                        </a>
                        <h5>Fortuner TRD</h5>

                        <div class="price">Giá từ 1,009 tỷ</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/TRD_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Fortuner TRD 2.7V (4x4)</span>
                                    <span>1,115 tỷ</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/TRD_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Fortuner TRD 2.7V (4x2)</span>
                                    <span>1,009 tỷ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/land.png" alt=""/>
                        </a>
                        <h5>Land Cruiser</h5>

                        <div class="price">Giá từ 2,702 tỷ</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/land_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Land Cruiser VX</span>
                                    <span>2,702 tỷ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- -->
            <div class="row_auto">
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/prado.png" alt=""/>
                        </a>
                        <h5>Land Cruiser Prado</h5>

                        <div class="price">Giá từ 2,071 tỷ</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/prado_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Land Cruiser Prado</span>
                                    <span>2,071 tỷ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/hilux.png" alt=""/>
                        </a>
                        <h5>Hilux</h5>

                        <div class="price">Giá từ 637 triệu</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/hilux_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Hilux 3.0G 4x4</span>
                                    <span>735 triệu</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/hilux_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Hilux 2.5E 4x2</span>
                                    <span>637 triệu</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_auto">
                    <div class="col_auto">
                        <div class="compare_click">So sánh</div>
                        <a class="auto_click" href="#" title="">
                            <img src="images/auto/xe/hiace.png" alt=""/>
                        </a>
                        <h5>Hiace</h5>

                        <div class="price">Giá từ 1,094 tỷ</div>
                    </div>
                    <div class="child_auto">
                        <div class="cars">
                            <img class="trigger" src="images/auto/hiace_1.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Hiace Động cơ dầu</span>
                                    <span>1,179 tỷ</span>
                                </div>
                            </div>
                        </div>
                        <div class="cars">
                            <img class="trigger" src="images/auto/hiace_2.png" alt=""/>
                            <input type="checkbox" name="check_car" value="0"/>

                            <div class="popup">
                                <div>
                                    <span>Hiace Động cơ xăng</span>
                                    <span>1,094 tỷ</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- -->
            <div class="r_menu">
                <div class="col_menu">
                    <h3>Dich Vu</h3>
                    <ul>
                        <li><a href="#" title="">Dat hen bao duong</a></li>
                        <li><a href="#" title="">Doi mau son xe</a></li>
                        <li><a href="#" title="">Bao duong nhanh</a></li>
                        <li><a href="#" title="">Phu kien lam dep</a></li>
                        <li><a href="#" title="">Cuu ho 24/24</a></li>
                    </ul>
                </div>

                <div class="col_menu">
                    <h3>Ho Tro</h3>
                    <ul>
                        <li><a href="#" title="">Du toan tai chinh</a></li>
                        <li><a href="#" title="">Huong dan thu tuc</a></li>
                        <li><a href="#" title="">Qui trinh thanh toan va giao xe</a></li>
                        <li><a href="#" title="">Phu kien lam dep</a></li>
                        <li><a href="#" title="">Chinh sach bao hanh</a></li>
                    </ul>
                </div>

                <div class="col_menu">
                    <h3>Thong Tin Chung</h3>
                    <ul>
                        <li><a href="#" title="">Khuyen mai</a></li>
                        <li><a href="#" title="">Hoat dong su kien</a></li>
                        <li><a href="#" title="">Tu van lai xe an toan</a></li>
                        <li><a href="#" title="">Huong dan cham soc va su dung xe</a></li>
                        <li><a href="#" title="">Tuyen dung</a></li>
                    </ul>
                </div>
            </div>
            <!-- End left menu-->
        </div>
    </div>
<!-- End Menu-->
</div>
<!-- End Header-->