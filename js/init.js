jQuery(function(){
	jQuery('#slide_home').camera({
		fx					: 'random',
		loader: 'none',
		height: '340px',
		thumbnails: false,	
		hover	: false,
		navigation	: false					
	});
	$('.col_auto').mouseover(
	function(){
		$(this).find('.compare_click').addClass('active1');
		}
	);
	$('.col_auto').mouseout(
	function(){
		$(this).find('.compare_click').removeClass('active1');
		}
	);
	var clicks = 0;
	$('.compare_click').live( "click", function(){				
		if(clicks%2==0){
			$('.price').hide();	
			$('.child_auto').slideToggle('slow');						
		}
		else
		{	
			$('.price').show();
			$('.child_auto').hide();					
		}
		++clicks;						
		});	
		 	
	$('.jcarousel').jcarouselAutoscroll({
    autostart: true
	});
	
	$('.col_news .item_news .click_news').hoverdir();	
	
	$('.hide_navar').live( "click", function(){			
  	if( $("#menu").is(":hidden") ) {
     $('#menu').slideToggle('slow');
	 $('.search').slideToggle('slow');
		$('#wrapper').hide();
		$('#footer').hide();
		$(this).addClass('active');
  	 }
   	else
   	{
		$('.search').hide();
        $('#menu').hide();
		$('#wrapper').show();
		$('#footer').show();
		$(this).removeClass('active');
  	 }
	});	
	$('#facebook').fancybox();	

	/*
	*	car check box change event
	*/
	$('.child_auto input[type="checkbox"]').change(function(){
		if ($(this).is(':checked'))
		{	
			$(this).parents('.item_auto').find('.auto_click').addClass('active');
		}
		else{
			if ($(this).parents('.child_auto').find('input[type="checkbox"]:checked').length == 0)
				$(this).parents('.item_auto').find('.auto_click').removeClass('active');

		}
	})
});